--------------
--[  Keys  ]--
--------------

vim.g.mapleader = " "
local keymap = vim.keymap

keymap.set("i", "jk", "<ESC>")
keymap.set("n", "x", '"_x')

-- window management
keymap.set("n", "<leader>sv", "<C-w>v") -- split window vertically
keymap.set("n", "<leader>sh", "<C-w>s") -- split window horizontally
keymap.set("n", "<leader>sq", ":close<CR>") -- close current split window

keymap.set("n", "<leader>tn", ":tabnew<CR>") -- open new tab
keymap.set("n", "<leader>tq", ":tabclose<CR>") -- close current tab

keymap.set("n", "<leader>wq", ":wq<CR>")
keymap.set("n", "<leader>w", ":w<CR>")
keymap.set("n", "<leader>qq", ":q!<CR>")
keymap.set("n", "<leader>ww", ":w!<CR>")

keymap.set("n", "<leader>fd", ":Telescope fd<CR>")

vim.opt.relativenumber = true

