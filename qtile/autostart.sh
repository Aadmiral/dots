#!/bin/bash

export PATH="$HOME/.config/Admiral/scripts:$PATH"
killall -9 xfsettingsd picom polybar mpd dunst ksuperkey xfce4-power-manager aw-qt aw-server

sxhkd -c "$HOME/.config/qtile/sxhkdrc" &
picom --config "$HOME/.config/qtile/picom.conf" &
dunst -conf "$HOME/.config/dunst/dunstrc_qtile"&

xfsettingsd &
xfce4-power-manager &
nm-applet &
xfce4-power-manager &

copyq &
volumeicon &
blueberry-tray &
emote &
thunar --daemon &
autojump &
aw-qt &

# xrandr --output DP1 --off &

setxkbmap -option "caps:escape" &
ksuperkey -e 'Super_L=F1' &
ksuperkey -e 'Super_R=F1' &

