import os, subprocess, random
from libqtile import layout, bar, widget, hook
from libqtile.config import Drag, Group, Key, ScratchPad, DropDown, Match, Screen
from libqtile.command import lazy
# import arcobattery

mod = "mod4"
home = os.path.expanduser('~')

color = [ "#e3e9fc", "#1e2128", "#50545B", "#5294e2"]

# Wallpaper
wallpapers_path = home + "/Pictures/Wallpapers/"
list_wallpapers = os.listdir(wallpapers_path)
wallpaper_image = random.choice(list_wallpapers)

@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

keys = [
    Key([mod, "shift"], "f", lazy.window.toggle_fullscreen()),
    Key([mod], "q", lazy.window.kill()),
    Key([mod, "control"], "r", lazy.restart()),
    Key([mod], "space", lazy.next_layout()),
    Key([mod, "shift"], "space", lazy.window.toggle_floating()),

    Key([mod], "Up", lazy.layout.up()),
    Key([mod], "Down", lazy.layout.down()),
    Key([mod], "Left", lazy.layout.left()),
    Key([mod], "Right", lazy.layout.right()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),

    Key([mod, "control"], "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),),
    Key([mod, "control"], "Right",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),),
    Key([mod, "control"], "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),),
    Key([mod, "control"], "Left",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),),
    Key([mod, "control"], "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),),
    Key([mod, "control"], "Up",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),),
    Key([mod, "control"], "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),),
    Key([mod, "control"], "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),),
    ]

def window_to_previous_screen(qtile, switch_group=False, switch_screen=False):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group, switch_group=switch_group)
        if switch_screen == True:
            qtile.cmd_to_screen(i - 1)

def window_to_next_screen(qtile, switch_group=False, switch_screen=False):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group, switch_group=switch_group)
        if switch_screen == True:
            qtile.cmd_to_screen(i + 1)

keys.extend([
    # MOVE WINDOW TO NEXT SCREEN
    Key([mod,"shift"], "Right", lazy.function(window_to_next_screen, switch_screen=True)),
    Key([mod,"shift"], "Left", lazy.function(window_to_previous_screen, switch_screen=True)),
])

groups = []
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
group_labels = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

lyt = "Columns"
group_layouts = [lyt, lyt, lyt, lyt, lyt, lyt, lyt, lyt, lyt]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for i in groups:
    keys.extend([
        Key([mod], i.name, lazy.group[i.name].toscreen()),
        Key([mod, "control"], i.name, lazy.window.togroup(i.name)),
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name) , lazy.group[i.name].toscreen()),
    ])


layout_theme = {
        "border_width": 2, 
        "border_focus": color[3], 
        "border_normal": color[1],}
layouts = [
    layout.Columns(**layout_theme),
    layout.Floating(**layout_theme),
]


# Define scratchpads
groups.append(ScratchPad("scratchpad", [
    DropDown("term", "kitty", width=0.8, height=0.8, x=0.1, y=0.1, opacity=1),
    # DropDown("superw", "alacritty -o 'window.opacity=1' 'font.size=8' -e python /home/admiral/Documents/Super+W/main.py", width=0.4, height=0.4, x=0.3, y=0.3),
    DropDown("Pavucontrol", "pavucontrol -t 3", width=0.4, height=0.5, x=0.3, y=0.26, opacity=1),
    DropDown("Feeds", "gfeeds", width=0.8, height=0.8, x=0.1, y=0.1, opacity=0.95),
]))

# Scratchpad keybindings
keys.extend([
    Key([mod], "z", lazy.group['scratchpad'].dropdown_toggle('term')),
    # Key([mod], "shift"], "w", lazy.group['scratchpad'].dropdown_toggle('superw')),
    Key([mod], "p", lazy.group['scratchpad'].dropdown_toggle('Pavucontrol')),
    # Key([mod], "f", lazy.group['scratchpad'].dropdown_toggle('Feeds')),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
])


def init_widgets_defaults():
    return dict(font="FiraCode Bold",
                fontsize = 18,
                padding = 2,
                foreground = color[0],
                background=color[1])
widget_defaults = init_widgets_defaults()

def init_widgets_list():
    widgets_list = [
                widget.Clock(
                    format='  %a  %h  %d  %I:%M  %p  ',
                ),
                widget.Spacer(),    
                widget.GroupBox(
                    highlight_method='line', #block 
                    this_current_screen_border=color[0],
                    highlight_color=color[1],
                    other_screen_border=color[2],
                    active=color[0],inactive=color[2],
                    font="Hack Bold",
                    fontsize = 20,
                    margin_x = 8,
                    padding_x = 4,
                    background=color[1]
                    ),
                widget.Prompt(),
                widget.Spacer(),    
                widget.WidgetBox(
                    widgets=[ widget.Systray(icon_size = 25,padding = 7,),],
                    font="Hack Bold", fontsize = 38, 
                    text_closed = "", text_open = " ",),
                # arcobattery.BatteryIcon(
                #          padding=0, scale=0.7, y_poss=2,
                #          theme_path=home + "/.config/qtile/icons/battery_icons_horiz",
                #          update_interval = 30,
                #  ),
                widget.Battery(
                    format=" {char} {percent:2.0%}  ",
                    low_percentage=0.2, update_interval=30,
                    charge_char='', discharge_char='', unknown_char = "",
                    low_foreground='#fb4934',
                ),
                widget.TextBox(
                    text = '', fontsize = 50, padding = -1,
                ),
              ]
    return widgets_list

widgets_list = init_widgets_list()
def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1

widgets_screen1 = init_widgets_screen1()
def init_screens():
    return [Screen(
        wallpaper = f"{wallpapers_path}{wallpaper_image}",
        wallpaper_mode="fill",
        bottom=bar.Bar(widgets=init_widgets_screen1(), size=28, opacity=1))
        ]

screens = init_screens()


# MOUSE CONFIGURATION
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size())
]

dgroups_key_binder = None
dgroups_app_rules = []

# ################ assgin apps to groups ##################
# @hook.subscribe.client_new
# def assign_app_group(client):
#     d = {}
#     d[group_names[2]] = ["Firefox","Brave", "Brave-browser", "firefox", "brave", "brave-browser"]
#     d[group_names[3]] = ["Gimp", "gimp"]
#
# wm_class = client.window.get_wm_class()[0]
#
#     for i in range(len(d)):
#         if wm_class in list(d.values())[i]:
#             group = list(d.keys())[i]
#             client.togroup(group)
#             client.group.cmd_toscreen(toggle=False)
#
# # END APPLICATIONS TO A SPECIFIC GROUPNAME


main = None

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr']) # ?????

@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types):
        window.floating = True

floating_types = ["notification", "toolbar", "splash", "dialog"]


follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(wm_class='confirm'),
    Match(wm_class='dialog'),
    Match(wm_class='download'),
    Match(wm_class='error'),
    Match(wm_class='file_progress'),
    Match(wm_class='notification'),
    Match(wm_class='splash'),
    Match(wm_class='toolbar'),
    Match(wm_class='Arandr'),
    Match(wm_class='feh'),
    Match(wm_class='blueberry.py'),
    Match(wm_class='copyq'),
    Match(wm_class='Galculator'),
    Match(wm_class='archlinux-logout'),
    Match(wm_class='xfce4-terminal'),

],  fullscreen_border_width = 0, border_width = 0)
auto_fullscreen = True

focus_on_window_activation = "focus" # or smart
wmname = "LG3D"

