#!/bin/bash

printf '''
________      _____        
___  __ \_______  /________
__  / / /  __ \  __/_  ___/
_  /_/ // /_/ / /_ _(__  ) 
/_____/ \____/\__/ /____/  

''' | lolcat

list=( "alacritty" \
  "dunst" \
  "fish" \
  "kitty" \
  "neofetch" \
  "nvim" \
  "openbox" \
  "qtile" \
  "qutebrowser" \
  "ranger" \
  "rofi" \
  "zellij" \
  "helix" \
  "wezterm" \
  "scripts")

for element in "${list[@]}"; do
  trash "$HOME/.config/$element"
  ln -s "$HOME/dots/$element" "$HOME/.config/$element"
  echo -n " [$element] " 

done

