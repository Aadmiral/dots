####################
##      FISH      ##
####################

set PATH "~/Desktop/flutter/bin" $PATH
set PATH "~/.cargo/bin" $PATH
set PATH "~/.emacs.d/bin" $PATH
set PATH "~/.config/Admiral/scripts" $PATH
set PATH "~/.config/emacs/bin" $PATH

set EDITOR nvim 
set fish_greeting
autojump | grep "something" &
printf %b '\e]4;4;#6495ed\a'

export FZF_DEFAULT_OPTS=" --bind='ctrl-t:execute(nvim {})+abort' --cycle --prompt ' ' --pointer '👉' --marker='✔' -m --height=20% --color=bg+:#395b91,gutter:-1 --no-separator"

# make CapsLock behave like Ctrl:
setxkbmap -option ctrl:nocaps &
# make short-pressed Ctrl behave like Escape:
xcape -e 'Control_L=Escape' &

## BINDS ##

bind \er 'echo " Ranger " && ranger ; xdotool key enter'
bind \eq 'exit'
# bind \ef 'the-way search'
bind \ea 'zellij attach ; xdotool key enter'
bind \ee 'echo " Neovim " && /usr/bin/nvim -c ": Telescope find_files" ; xdotool key enter'
bind \ef 'echo "🚀" && cd $(pwd | find -maxdepth 3 | fzf) && sleep 0.1 && xdotool key enter'
bind \eF 'echo "🚀" && cd $(echo $HOME | find -maxdepth 3 | fzf ) && sleep 0.1 && xdotool key enter'

## ALIAS ##

alias ..='cd ..'
alias ...='cd ../..'

alias vi="nvim"
alias vim="nvim ./"
alias r="ranger ./"
alias conf="ranger $HOME/dots/"

alias cat="bat -p --theme=OneHalfDark"
alias ls="exa --icons"
alias la="exa --icons -la"
alias tree="exa --icons --tree"
alias pwd="/bin/pwd | lolcat ; /bin/pwd | xclip -selection clipboard"

alias vol="amixer set 'Master' "
alias br="xrandr --output eDP1 --brightness "
alias mo="xrandr --output DP1 --mode 1920x515 --pos 0x1080 &&  xrandr --output DP1 --brightness 0.7"
alias live="$HOME/dots/scripts/live && sleep 0.5"
alias icat="kitty +kitten icat"

alias androidstudio="~/Desktop/android-studio/bin/studio.sh "


## SCRIPTS ##

if status is-interactive
    # Commands to run in interactive sessions can go here
end


# MY Functions

function md -d "Create a directory and set CWD"
    command mkdir $argv
    if test $status = 0
        switch $argv[(count $argv)]
            case '-*'

            case '*'
                cd $argv[(count $argv)]
                return
        end
    end
end


# BANG BANG
function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end

bind ! __history_previous_command
bind '$' __history_previous_command_arguments

# set up the same key bindings for insert mode if using fish_vi_key_bindings
if test "$fish_key_bindings" = 'fish_vi_key_bindings'
    bind --mode insert ! __history_previous_command
    bind --mode insert '$' __history_previous_command_arguments
end

function _plugin-bang-bang_uninstall --on-event plugin-bang-bang_uninstall
    bind --erase --all !
    bind --erase --all '$'
    functions --erase _plugin-bang-bang_uninstall
end


# Function for creating a backup file
function backup --argument filename
    cp $filename $filename.bak
end

function cd --description "auto ls for each cd"
    if [ -n $argv[1] ]
        builtin cd $argv[1]
        and exa --icons 
    else
        builtin cd ~
        and exa --icons 
    end
end
