local wezterm = require 'wezterm'

return {
    initial_rows = 34,
    initial_cols = 140,
    wsl_domains = {
        {
          name = 'WSL:arch',
          distribution = 'arch',
          default_cwd = "/mnt/c/Users/User/Desktop",
        },
    },
    default_domain = 'WSL:arch',
    font = wezterm.font 'Hack Nerd Font',
    color_scheme = 'Catppuccin Mocha',
    
    automatically_reload_config = true,
    window_decorations = "RESIZE",

    window_frame = {
      active_titlebar_bg = '#282a36',
      inactive_titlebar_bg = '#282a36',
    },
    colors = {
      foreground = "#ffffff",
      background = "#282a36",

      tab_bar = {
        inactive_tab_edge = '#282a36',
        active_tab = {
            bg_color = "#454c5a",
            fg_color = "#ffffff",
            intensity = "Normal",
            underline = 'Single',
        },
        inactive_tab = {
            bg_color = "#282a36",
            fg_color = "#f8f8f2"
        },
        inactive_tab_hover = {
            bg_color = "#000000",
            fg_color = "#f8f8f2",
            italic = true
        },
        new_tab = {
            bg_color = "#282a36",
            fg_color = "#f8f8f2"
        },
        new_tab_hover = {
            bg_color = "#454c5a",
            fg_color = "#f8f8f2",
            italic = true
        }
      },
    },
      keys = {
        
        {
          mods = "ALT", key = "1", action = wezterm.action.ActivateTab(0)
        },
        {
          mods = "ALT", key = "2", action = wezterm.action.ActivateTab(1)
        },
        {
          mods = "ALT", key = "3", action = wezterm.action.ActivateTab(2)
        },
        {
          mods = "ALT", key = "4", action = wezterm.action.ActivateTab(3)
        },
        {
          mods = "ALT", key = "5", action = wezterm.action.ActivateTab(4)
        },
        {
          mods = "ALT", key = "6", action = wezterm.action.ActivateTab(5)
        },
        
        {
          key = "Enter",
          mods = "ALT",
          action = wezterm.action.SpawnTab "CurrentPaneDomain"
        },
        {
            key = "s",
            mods = "CTRL|SHIFT",
            action = wezterm.action.ShowLauncher
        },
        {
            key = "q",
            mods = "CTRL|SHIFT",
            action = wezterm.action.CloseCurrentTab { confirm = false}
        },
        {
            key = "v",
            mods = "CTRL",
            action = wezterm.action.PasteFrom 'Clipboard'
        },
        {
          key = 'n',
          mods = 'ALT',
          action = wezterm.action.SplitHorizontal { domain = 'CurrentPaneDomain' },
        },
        {
          key = 'N',
          mods = 'ALT',
          action = wezterm.action.SplitVertical { domain = 'CurrentPaneDomain' },
        },
        {
          key = 'LeftArrow',
          mods = 'CTRL',
          action = wezterm.action.ActivatePaneDirection 'Left',
        },
        {
          key = 'DownArrow',
          mods = 'CTRL',
          action = wezterm.action.ActivatePaneDirection 'Down',
        },
        {
          key = 'UpArrow',
          mods = 'CTRL',
          action = wezterm.action.ActivatePaneDirection 'Up',
        },
        {
          key = 'RightArrow',
          mods = 'CTRL',
          action = wezterm.action.ActivatePaneDirection 'Right',
        },
        {
          key = 'q',
          mods = 'CTRL',
          action = wezterm.action.PaneSelect { alphabet = '1234567890'}
        },
        {
          key = 'f',
          mods = 'CTRL',
          action = wezterm.action.TogglePaneZoomState
        },
    },
    launch_menu = {
      {
          label = "Arch WSL",
          args = {"wsl", "-d", "Arch"},
      },
      {
          label = "Powershell",
          args = {"powershell"},
      },
  },
  window_background_opacity = 1,
}
